$Username = "obscura"
$Password = "obscura"
$Comps = "10.42.178.190"
$Script = { Invoke-Expression -Command "c:\scripts\switch_external_device_bat.bat" }

#Create credential object
$SecurePassWord = ConvertTo-SecureString -AsPlainText $Password -Force
$Cred = New-Object -TypeName "System.Management.Automation.PSCredential" -ArgumentList $Username, $SecurePassWord

#Create session object with this
$Session = New-PSSession -ComputerName $Comps -credential $Cred

foreach ($comp in $Comps){
Invoke-Command -Session $Session -Scriptblock $Script
}
#$Null = Wait-Job -job $job

#Close Session
Remove-PSSession -Session $Session