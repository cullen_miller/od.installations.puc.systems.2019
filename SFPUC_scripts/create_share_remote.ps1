﻿$User = “obscura”
$Pass = ConvertTo-SecureString “obscura” -AsPlainText -Force
$Credentials = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User,$Pass

$Computers = Get-Content "c:\scripts\computers.txt"


#$session = New-PSSession -ComputerName $computer -Credential $Credentials
#Enter-PSSession $session

#Invoke-command -session $session -scriptblock{ New-Item "c:\Users\obscura\apps" -itemtype directory }

#Start-Sleep 2

#Remove-PSSession -Session $session
foreach ($Computer in $Computers){
$name = "deploy"
$path = "C:\projects\PUC\Deploy"
$Class=”WIN32_Share”
$Method = "Create"
$sd = ([WMIClass] "\\$Computer\root\cimv2:Win32_SecurityDescriptor").CreateInstance()
$ACE = ([WMIClass] "\\$Computer\root\cimv2:Win32_ACE").CreateInstance()
$Trustee = ([WMIClass] "\\$Computer\root\cimv2:Win32_Trustee").CreateInstance()
$Trustee.Name = "EVERYONE"
$Trustee.Domain = $Null
$Trustee.SID = @(1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0)
$ace.AccessMask = 2032127
$ace.AceFlags = 3
$ace.AceType = 0
$ACE.Trustee = $Trustee
$sd.DACL += $ACE.psObject.baseobject 
$Shares = [WmiClass]"\\$Computer\root\CIMV2:$Class"
$InParams = $Shares.psbase.GetMethodParameters($Method)
$InParams.Access = $sd
$InParams.Description = $description
$InParams.MaximumAllowed = $Null
$InParams.Name = $name
$InParams.Password = $Null
$InParams.Path = $path
$InParams.Type = [uint32]0
$R = $Shares.PSBase.InvokeMethod($Method, $InParams, $Null)
switch ($($R.ReturnValue))
 {
  0 {Write-Host "Share:$name Path:$path Result:Success"; break}
  2 {Write-Host "Share:$name Path:$path Result:Access Denied" -foregroundcolor red -backgroundcolor yellow;break}
  8 {Write-Host "Share:$name Path:$path Result:Unknown Failure" -foregroundcolor red -backgroundcolor yellow;break}
  9 {Write-Host "Share:$name Path:$path Result:Invalid Name" -foregroundcolor red -backgroundcolor yellow;break}
  10 {Write-Host "Share:$name Path:$path Result:Invalid Level" -foregroundcolor red -backgroundcolor yellow;break}
  21 {Write-Host "Share:$name Path:$path Result:Invalid Parameter" -foregroundcolor red -backgroundcolor yellow;break}
  22 {Write-Host "Share:$name Path:$path Result:Duplicate Share" -foregroundcolor red -backgroundcolor yellow;break}
  23 {Write-Host "Share:$name Path:$path Result:Reedirected Path" -foregroundcolor red -backgroundcolor yellow;break}
  24 {Write-Host "Share:$name Path:$path Result:Unknown Device or Directory" -foregroundcolor red -backgroundcolor yellow;break}
  25 {Write-Host "Share:$name Path:$path Result:Network Name Not Found" -foregroundcolor red -backgroundcolor yellow;break}
  default {Write-Host "Share:$name Path:$path Result:*** Unknown Error ***" -foregroundcolor red -backgroundcolor yellow;break}
}


net view \\$computer
}

start-sleep 2