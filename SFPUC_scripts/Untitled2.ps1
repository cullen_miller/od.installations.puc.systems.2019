﻿
$User = “obscura”
$Pass = ConvertTo-SecureString “obscura” -AsPlainText -Force
$Credentials = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User,$Pass

$computers = Get-Content "C:\scripts\computers.txt"

foreach ($computer in $computers) {

Invoke-Command -ComputerName $computer -ScriptBlock{get-psdrive -Name P | Remove-PSDrive} -cred $Credentials

}