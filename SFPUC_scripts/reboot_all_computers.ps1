$User = “obscura”
$Pass = ConvertTo-SecureString “obscura” -AsPlainText -Force
$Credentials = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User,$Pass

$Computers = Get-Content "c:\scripts\computers.txt"
#$MacAddr = Get-Content "c:\obscura\scripts\macs.txt"
#$tool = "C:\obscura\sysinternals\wol.exe"

#$Scriptblock1 = -scriptblock { Stop-Service -InputObject monsvc -Force}
#$Scriptblock2 = -scriptblock { Get-process "TouchDesigner" | stop-process -force}

#$MACS = import-csv "C:\obscura\scripts\computers.csv"


foreach ($computer in $Computers) { 

if (Test-Connection  -computername $computer -ErrorAction SilentlyContinue -quiet) {

#Invoke-Command -ComputerName $computer -ScriptBlock{ Get-service monsvc | Stop-service -force } 
#Invoke-Command -ComputerName $computer -ScriptBlock{ Get-process ODTouchServer | Stop-Process -force } 
#Invoke-Command -ComputerName $computer -ScriptBlock{ Get-process TouchDesigner | Stop-Process -force }
Invoke-Command -ComputerName $computer -ScriptBlock{ restart-Computer -force } -Credential $Credentials
    
    }

else {

  foreach ($addr in $MacAddr) {
  #Build the command string using a csv file
    
    #Run the command
    Invoke-Expression "$tool $addr"
    }
}
}